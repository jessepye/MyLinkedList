public class Node {
    private Object value;
    private Node next;

    public Object getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public Node getNext() {
        return next;
    }

    public void setNext(Node next) {
        this.next = next;
    }

    public Node(Object value) {
        this.value = value;
    }
}
