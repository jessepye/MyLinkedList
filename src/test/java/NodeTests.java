import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NodeTests {
    Node node;
    @BeforeEach
    public void setup() {
        node = new Node(123);
    }

    @Test
    public void nodeConstructorTest(){
        int expected = 123;

        Object result = node.getValue();

        assertEquals(expected, result, "Initializes a new node with node.value = 123");
    }
}